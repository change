// file      : foo.cxx -*- C++ -*-
// copyright : Copyright (c) 2014-2016 Code Synthesis Ltd
// license   : MIT; see accompanying LICENSE file

//#include <string>

#include <foo>

//using namespace std;

int
foo_value (int v)
{
  return v * 10;
}

foo::
foo (int v)
    : v_ (v)
{
}

int foo::
value ()
{
  return v_ * 100;
}

int foo::
macro ()
{
  return MODTEST_MACRO;
}

/*
string foo::
message (const char* s) const
{
  return string ("foo: ") + s;
}
*/
