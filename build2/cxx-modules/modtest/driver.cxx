// file      : driver.cxx -*- C++ -*-
// copyright : Copyright (c) 2014-2016 Code Synthesis Ltd
// license   : MIT; see accompanying LICENSE file

//#include <string>
#include <iostream>

// For macro isolation test.
//
#define MODTEST_MACRO 2

// VC introduces module-related declarations (import, export, module). CLang
// doesn't do that implementing some transitional model interpreting headers as
// modules according to a special "module.modulemap" file and performing import
// in place of #include.
//
#if defined(MODTEST_USE_MODULES) && defined(_MSC_VER)

import foo;
import bar;

#else

#  include <foo>
#  include <bar>

// If we are using modules and #include directive is effectivelly converted to
// the import declaration there should be no "redefinition of foo" error. Let's
// check that.
//
#  ifdef MODTEST_USE_MODULES
#    include <foo>
#  endif

#endif

using namespace std;

int
main ()
{
  foo f (3);
  cerr << "foo values: " << foo_value (5) << " " << f.value () << endl
       << "foo macros: "
       << (MODTEST_MACRO == f.macro () ? "leaks" : "isolated") << endl;

//   cerr << f.message ("Hi") << endl;

  bar b (7);
  cerr << "bar values: " << bar_value (6) << " " << b.value () << endl;
}
