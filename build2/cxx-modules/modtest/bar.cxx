// file      : bar.cxx -*- C++ -*-
// copyright : Copyright (c) 2014-2016 Code Synthesis Ltd
// license   : MIT; see accompanying LICENSE file

//#include <string>

#include <bar>

//using namespace std;

int
bar_value (int v)
{
  return v * 100;
}

bar::
bar (int v)
    : v_ (v)
{
}

int bar::
value ()
{
  return v_ * 1000;
}

/*
string bar::
message (const char* s) const
{
  return string ("bar: ") + s;
}
*/
